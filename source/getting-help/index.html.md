---
layout: markdown_page
title: "Getting Help for GitLab"
---


## Help Topics
{:.no_toc}

- TOC
{:toc}

---

## Updating
* [GitLab update page](https://about.gitlab.com/update/): resources and information to help you update your GitLab instance.
* [Maintenance policy](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/MAINTENANCE.md): specifies what versions are supported.

## Discussion

If you'd like to discuss a feature proposal or a bug found within the product. Please raise an issue to the offical GitLab issue trackers:

### Feature Proposals

Feature proposals should be submitted to the issue tracker of the relevant product:

* [GitLab CE Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues)
* [GitLab EE Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues)

Please read the [contributing guidelines for feature proposals](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#feature-proposals) before posting on the Issue tracker and make use of the "Feature Proposal" issue template.

### Reproducible Bugs

Bug reports should be submitted to the issue tracker of the relevant product:

* [GitLab CE Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues)
* [GitLab EE Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues)

Please read the [contributing guidelines for reporting bugs](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#issue-tracker-guidelines) before posting on the Issue tracker and make use of the "Bug" issue template.

Other resources for discussion:

* [#gitlab IRC channel](https://webchat.freenode.net/?channels=gitlab): a Freenode channel to get in touch with other GitLab users and get help. It is managed by James Newton (newton) and Drew Blessing (dblessing).
* [GitLab Community Forum](https://forum.gitlab.com/): this is the best place to have a discussion.
* [Mailing list](https://groups.google.com/forum/#!forum/gitlabhq): Please search for similar issues before posting your own, there's a good chance somebody else had the same issue you have now and has resolved it.
* [Gitter chat room](https://gitter.im/gitlabhq/gitlabhq#): here you can ask questions when you need help.

## Technical Support

### GitLab Community Edition

If you are seeking help with your GitLab Community Edition installation, please use the following resources:

* [GitLab Documentation](https://docs.gitlab.com): Extensive Documentation is available regarding the Supported Configurations of GitLab.
* [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab): Please search for similar issues before posting your own, there's a good chance somebody else had the same issue you have now and has resolved it.
* [GitLab Community Forum](https://forum.gitlab.com/): this is the best place to have a discussion about your chosen configuration and options.

### GitLab Enterprise Edition

If you are seeking help with your GitLab Enterprise Edition installation,
please use the following resources:

* [Subscription](https://about.gitlab.com/pricing/): allows you to contact our GitLab service engineers to solve your problem.
* [Consultancy](https://about.gitlab.com/consultancy/): personalized attention from the GitLab experts for installations, upgrades and customizations.

### GitLab.com Service

If you are seeking help regarding the service provided at GitLab.com, please use the following resources:

* [GitLab.com Support Tracker](https://gitlab.com/gitlab-com/support-forum/issues) to report issues and bugs if you do not have a [Premium Support subscription](https://about.gitlab.com/pricing/#gitlab-ee).

## Security
* [The responsible disclosure page](https://about.gitlab.com/disclosure/) describes how to contact GitLab to report security vulnerabilities and other security information.
* [The security section in the documentation](http://doc.gitlab.com/ce/security/README.html) lists what you can do to further secure your GitLab instance.

## Contributing
* [Contributing guide](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md): describes how to submit Merge Requests and issues. Pull Requests and Issues not in line with the guidelines in this document will be closed.

## Reference
* [GitLab University](https://university.gitlab.com/) contains a variety of resources.
* [GitLab CE documentation](http://doc.gitlab.com/ce/): information about the functionality in GitLab for users, administrators and developers.
* [GitLab documentation](https://about.gitlab.com/documentation/) contails links to documentation for all GitLab applications
* [The GitLab Cookbook](https://www.packtpub.com/application-development/gitlab-cookbook): written by core team member Jeroen van Baarsen, it is the most comprehensive book about GitLab.
* [GitLab Recipes](https://gitlab.com/gitlab-org/gitlab-recipes) A range of useful unofficial guides to using non-packaged software in conjunction with GitLab.
* [Learn Enough Git to Be Dangerous by Michael Hartl](http://www.learnenough.com/git-tutorial) is a great introduction to version control and git.
* Version two of the [Pro Git book](http://git-scm.com/book/en/v2) has a [section about GitLab.](http://git-scm.com/book/en/v2/Git-on-the-Server-GitLab)
* O'Reilly Media 'Git for teams' [book](http://shop.oreilly.com/product/0636920034520.do) has a chapter on GitLab, there are also [videos](http://shop.oreilly.com/product/0636920034872.do?code=WKGTVD) about Git and GitLab. They also provide a [free video about creating a GitLab account](http://player.oreilly.com/videos/9781491912003?toc_id=194077).
* [GitLab Youtube Channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg): the place where you can see video's of features and installation options.
